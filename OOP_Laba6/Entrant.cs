﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba6
{
    public class Entrant
    {
        private string FullName { get; set; }
        private string IdNum { get; set; }
        private double AvgPoints { get; set; }
        private bool IsAwarded { get; set; }
        private ZNO[] ZNOResults { get; set; }

        public Entrant() { }

        public Entrant(string fullName, string idNum, double avgPoints, bool isAwarded, ZNO[] znoResults)
        {
            FullName = fullName;
            IdNum = idNum;
            AvgPoints = avgPoints;
            IsAwarded = isAwarded;
            ZNOResults = znoResults;
        }

        public Entrant(Entrant otherEntrant)
        {
            FullName = otherEntrant.FullName;
            IdNum = otherEntrant.IdNum;
            AvgPoints = otherEntrant.AvgPoints;
            IsAwarded = otherEntrant.IsAwarded;
            ZNOResults = otherEntrant.ZNOResults;
        }

        public string GetBestSubject()
        {
            double maxPoints = -1;
            string bestSubject = null;

            foreach (var zno in ZNOResults)
            {
                if (zno.Points > maxPoints)
                {
                    maxPoints = zno.Points;
                    bestSubject = zno.Subject;
                }
            }

            return bestSubject;
        }

        public bool IsOnTopOfTheRating()
        {
            return IsAwarded && AvgPoints >= 4.9;
        }

        public override string ToString()
        {
            return $"Повне імя: {FullName}, ID: {IdNum}, Середній бал: {AvgPoints}, Нагороджувався: {IsAwarded}, Найкращій предмет: {GetBestSubject()}";
        }
    }

}
