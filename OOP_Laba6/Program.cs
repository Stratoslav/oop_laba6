﻿using System;
using OOP_Laba6;

public class Program
{
    static Entrant[] CreateEntrants(int n)
    {
        Entrant[] entrants = new Entrant[n];
        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"\r\nВведіть дані для учасника {i + 1}:");
            Console.Write("Повне імя: ");
            string fullName = Console.ReadLine();
            Console.Write("ID: ");
            string idNum = Console.ReadLine();
            Console.Write("Середній бали: ");
            double avgPoints = Convert.ToDouble(Console.ReadLine());
            Console.Write("Нагороджувався (true/false): ");
            bool isAwarded = Convert.ToBoolean(Console.ReadLine());

            Console.Write("Введіть кількість результатів ЗНО: ");
            int znoCount = Convert.ToInt32(Console.ReadLine());
            ZNO[] znoResults = new ZNO[znoCount];
            for (int j = 0; j < znoCount; j++)
            {
                Console.Write($"Предмет ЗНО {j + 1}: ");
                string subject = Console.ReadLine();
                Console.Write($"Бали ЗНО з предмету {j + 1}: ");
                int points = Convert.ToInt32(Console.ReadLine());
                znoResults[j] = new ZNO(subject, points);
            }

            entrants[i] = new Entrant(fullName, idNum, avgPoints, isAwarded, znoResults);
        }

        return entrants;
    }

    static void PrintEntrantDetails(Entrant entrant)
    {
        Console.WriteLine(entrant);
    }

    static void PrintAllEntrants(Entrant[] entrants)
    {
        foreach (var entrant in entrants)
        {
            PrintEntrantDetails(entrant);
        }
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Введіть кількість учасників: ");
        int n = Convert.ToInt32(Console.ReadLine());

        Entrant[] entrants =    (n);

        while (true)
        {
            Console.WriteLine("\nMenu:");
            Console.WriteLine("1. Роздрукувати дані конкретного Учасника");
            Console.WriteLine("2. Роздрукувати інформацію про всіх Учасників\r\n");
            Console.WriteLine("3. Вийти");

            Console.Write("Виберіть дію (1/2/3): ");
            int option = Convert.ToInt32(Console.ReadLine());

            switch (option)
            {
                case 1:
                    Console.Write("Введіть номер учасника: ");
                    int entrantNumber = Convert.ToInt32(Console.ReadLine());
                    if (entrantNumber >= 1 && entrantNumber <= n)
                    {
                        PrintEntrantDetails(entrants[entrantNumber - 1]);
                    }
                    else
                    {
                        Console.WriteLine("Недійсний номер учасника.");
                    }
                    break;

                case 2:
                    PrintAllEntrants(entrants);
                    break;

                case 3:
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("Недійсний варіант.");
                    break;
            }
        }
    }
}
