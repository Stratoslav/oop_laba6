﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba6
{
    public class ZNO
    {
        private string Subject { get; set; }
        private int Points { get; set; }

        public ZNO(string subject, int points)
        {
            Subject = subject;
            Points = points;
        }
    }
}
